// const config = require("~/configs/config.gobal")

importScripts('https://www.gstatic.com/firebasejs/8.2.7/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/8.2.7/firebase-messaging.js')

// Initialize the Firebase app in the service worker by passing in the
firebase.initializeApp({
  apiKey: "AIzaSyAhjgTYncviTr14UbtFffG7XVTNFXeG8SY",
  authDomain: "document-management-9651e.firebaseapp.com",
  projectId: "document-management-9651e",
  storageBucket: "document-management-9651e.appspot.com",
  messagingSenderId: "583601590639",
  appId: "1:583601590639:web:0c26f6a806ae6ba545189a",
  measurementId: "G-WR5362HW6K",
});

// Retrieve firebase messaging
const messaging = firebase.messaging()
messaging.onBackgroundMessage(function (payload) {
  // console.log('Received background message ', payload)
  // const notificationTitle = payload.notification.title
  // const notificationOptions = {
  //   body: payload.notification.body,
  // }
  // self.registration.showNotification(notificationTitle, notificationOptions)
})
