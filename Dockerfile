FROM node:16-alpine 

RUN mkdir -p /var/www/dockerize-nuxt/nuxt-app
WORKDIR /var/www/dockerize-nuxt/nuxt-app

COPY package*.json ./
RUN npm install

COPY . .

RUN npm run generate


# EXPOSE 4505

ENV NUXT_HOST=0.0.0.0

ENV NUXT_PORT=6500

CMD [ "npm", "start" ]

